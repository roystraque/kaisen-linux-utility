# Kaisen linux & CAS AGENCY

Dans cette rubrique, nous allons énumérer toutes les informations utiles pour utiliser la distribution en tant qu'environnement de développement ou l'utilisée aussi dans un environnement d'audit de sécurité, grâce à des utilitaires déjà intégré à celle-ci ou développer par d'autres personnes. Nous vous regroupons ici toute les information et commande utile pour faire cela


Modification du dépot kaisen linux UPDATE
-----------------------------------------
Avant d'effectuer les actions qui vont suivre ou les astuces, sachez que vous devez impérativement modifier le dépôt kaisen linux remplacer le comme ceci :

```
deb https://deb.kaisenlinux.org kaisen-rolling main contrib non-free non-free-firmware
deb-src https://deb.kaisenlinux.org kaisen-rolling main contrib non-free non-free-firmware
```
![dépot kaisen modif](img/depot-kaisen-modif.png)




Mise en place exegol en version intégrale
-----------------------------------------

exegol est un pack d'outils développer pour pouvoir utiliser tous les outils de kali Linux sur la distribution kaisen, cependant celui-ci n'est pas encore à mon sens finalisé, mais a le mérite de pouvoir regrouper tous les outils utils pour de l'audit de sécurité.
Pour cela, il vous suffit de l'installer comme suite.

```
sudo exegol install full -y
sudo /usr/share/kaisen-services-management/docker-enable

```
![Ajout alias](img/exegol.png)

Vous pouvez aussi créer dans .zhrc des alias utiles pour lancer le doker car exegol en a besoin pour fonctionner.
```
alias meta="exegol start full"
alias dockerstop="sudo /usr/share/kaisen-services-management/docker-disable"
alias dockerstart="sudo /usr/share/kaisen-services-management/docker-enable"
```
![Ajout alias](img/alias-zhrc.jpg)



Mise en place de conky
----------------------

```
sudo apt install kaisen-conky
sudo kaisen-update
cp ~/.conkyrc ~/.myconkyconf && sudo sed -i 's/\/usr\/bin\/conky/\/usr\/bin\/conky --config ~\/.myconkyconf/g' /etc/xdg/autostart kaisen-conky.desktop
```


Voici personnellement les modifications pour faire en sorte que cela prenne toute la hauteur de votre écran, légèrement transparent.

```
https://gitlab.com/roystraque/kaisen-linux-utility/-/raw/main/conky
```



Mise en place de workstation-17.0.1
------------------------------------
```
sudo apt install git
git checkout workstation-17.0.1
make -j $(cat /proc/cpuinfo | grep ^processor | wc -l)
sudo make install
```
Licence key: MC60H-DWHD5-H80U9-6V85M-8280D




Mise en place de wireguard
--------------------------
(.deb disponible dans le projet pour la version gui)

```
sudo apt install wireguard
sudo dpkg -i wireguird_amd64.deb
```
![Ajout alias](img/wireguird.png)



Mise en place du logiciel IPTV
-------------------------------
(.deb disponible dans le projet pour la version gui)

```
sudo dpkg -i hypnotix_3.2_all.deb
```
![hypnotix gui kaisen linux](img/hypnotix-img02.jpg)




Mise en place du logiciel de mail
----------------------------------

```
sudo dpkg -i mailspring-1.10.8-amd64.deb
```



Mise en place du logiciel SSH version termius
---------------------------------------------

```
sudo dpkg -i termius.deb
```
![Ajout termius](img/sshtermius.jpg)




Mise en place de Sublime-Text 4 version 4143
--------------------------------------------

```
cd /etc/apt/sources.list.d/
sudo nano sublime-text.list
Coller : deb https://download.sublimetext.com/ apt/stable/
sudo wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - 
sudo apt kaisen-update && sudo apt install sublime-text
```

```
cd /opt/sublime_text/
sudo mv sublime_text sublime_text.back
sudo wget https://gitlab.com/roystraque/kaisen-linux-utility/-/raw/main/sublime_text
sudo chmod +x sublime_text
```

![Ajout termius](img/sublime-text.png)




Mise en place gWakeOnLAN
------------------------

```
wget https://gitlab.com/roystraque/kaisen-linux-utility/-/blob/main/gwakeonlan_0.8.5-1_all.deb
sudo dpkg -i gwakeonlan_0.8.5-1_all.deb

```


![Ajout reseaux](img/Wake-on-LAN.png)




Solution erreur pip search
---------------------------

```
wget https://gitlab.com/roystraque/kaisen-linux-utility/-/raw/main/pypi-simple-search-master.zip
unzip pypi-simple-search-master.zip
cd pypi-simple-search-master/bin/
chmod +x pip-pss pypi-simple-search

```


```
cd $HOME && sudo nano .zshrc

Add to alias : alias pip="$HOME/pypi-simple-search/bin/pip-pss"


```


![Ajout pipsearch](img/pipSearch-alias-install.png)




Mise en place de ChatGPT
-------------------------

```
wget https://gitlab.com/roystraque/kaisen-linux-utility/-/raw/main/ChatGPT_0.12.0_linux_x86_64.deb
sudo dpkg -i ChatGPT_0.12.0_linux_x86_64.deb

```
Un lien est alors créer dans votre menu GNU/Linux


![Ajout ChatGPT](img/chatgpt.png)




Installation de WhatsApp
-------------------------

```
wget https://gitlab.com/roystraque/kaisen-linux-utility/-/blob/main/whatsapp-for-linux_1.5.1_amd64.deb
sudo dpkg -i whatsapp-for-linux_1.5.1_amd64.deb

```

![Ajout WhatsApp](img/WhatsApp-install.png)

![Ajout WhatsApp](img/WhatsApp-install-02.png)

